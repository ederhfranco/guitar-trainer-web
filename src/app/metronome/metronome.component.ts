import { Component, OnInit, Input } from '@angular/core';
import * as Tone from 'tone';
import StartAudioContext from 'startaudiocontext';

export enum Toggle {
  On,
  Off,
}

@Component({
  selector: 'app-metronome',
  templateUrl: './metronome.component.html',
  styleUrls: ['./metronome.component.scss']
})
export class MetronomeComponent implements OnInit {

  beatsPerMinute = 120;
  status = Toggle.Off;

  @Input()
  displayAndControlVisualElements = true;

  constructor() { }

  ngOnInit(): void {

    this.setup();

  }

  setup(): void {
    StartAudioContext(Tone.context, document.documentElement);
    if (Tone.context.state !== 'running') {
      Tone.context.resume();
    }

    // this.setBpm(this.beatsPerMinute);

    const tickSynth = this.buildTick();

    Tone.Transport.scheduleRepeat((time) => {
      tickSynth.triggerAttackRelease("B5", "16n", time);
    }, '4n');
  }

  buildTick(): any {
    const tickSynth =  new Tone.Synth().toMaster();
    tickSynth.oscillator.type = 'sine';
    tickSynth.envelope.attack = 0;
    tickSynth.envelope.decay = 0.05;
    tickSynth.envelope.sustain = 0;
    tickSynth.envelope.release = 0.001;

    tickSynth.toMaster();

    return tickSynth;
  }

  start(): void {
    Tone.Transport.start();
  }

  stop(): void {
    Tone.Transport.stop();
    Tone.Transport.position = 0;
  }

  addBpm(value: number): void {
    this.setBpm(this.beatsPerMinute + value);
  }

  setBpm(bpm: number): void {
    this.beatsPerMinute = bpm;
    Tone.Transport.bpm.value = bpm;
    this.buildTick();
  }

  updateBpm(): void {
    this.setBpm(this.beatsPerMinute);
    this.stop();
    this.start();
  }
}
