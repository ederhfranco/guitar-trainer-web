import { MetronomeComponent } from './../metronome/metronome.component';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AutomatorComponent } from './automator.component';

describe('AutomatorComponent', () => {
  let component: AutomatorComponent;
  let fixture: ComponentFixture<AutomatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomatorComponent, MetronomeComponent ],
      imports: [ FormsModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  fdescribe('Testing generateSteps method', () => {
    it('With fullDuration 100 and stepDuration 55, should generate two steps: First 55 and second 45', () => {
      // Arrange
      component.fullDuration = 100;
      component.stepDuration = 55;

      // Act
      component.generateSteps();

      // Assert
      expect(component.steps.length).toBe(2);
      expect(component.steps[0].duration).toBe(55);
      expect(component.steps[1].duration).toBe(45);
    });

    it('With fullDuration 150 and stepDuration 55, should generate three steps: 55, 55 and 45', () => {
      // Arrange
      component.fullDuration = 150;
      component.stepDuration = 55;
  
      // Act
      component.generateSteps();
  
      // Assert
      expect(component.steps.length).toBe(3);
      expect(component.steps[0].duration).toBe(55);
      expect(component.steps[1].duration).toBe(55);
      expect(component.steps[2].duration).toBe(40);
    });

    it('With fullDuration 150 and stepDuration 50, should generate three steps: 50, 50 and 50', () => {
      // Arrange
      component.fullDuration = 150;
      component.stepDuration = 50;
  
      // Act
      component.generateSteps();
  
      // Assert
      expect(component.steps.length).toBe(3);
      expect(component.steps[0].duration).toBe(50);
      expect(component.steps[1].duration).toBe(50);
      expect(component.steps[2].duration).toBe(50);
    });

    it('When generate steps by second time, should delete old steps', () => {
      // Arrange
      component.fullDuration = 150;
      component.stepDuration = 50;
  
      // Act
      component.generateSteps();
      component.generateSteps();
  
      // Assert
      expect(component.steps.length).toBe(3);
      expect(component.steps[0].duration).toBe(50);
      expect(component.steps[1].duration).toBe(50);
      expect(component.steps[2].duration).toBe(50);
    });

    it('With right configuration, should generate right BPM for 2 steps, pair bpm difference pair', () => {
      // Arrange
      component.fullDuration = 100;
      component.stepDuration = 55;
      component.beginBpm = 100;
      component.finalBpm = 120;

      // Act
      component.generateSteps();

      // Assert
      expect(component.steps.length).toBe(2);
      expect(component.steps[0].beatsPerMinute).toBe(100);
      expect(component.steps[1].beatsPerMinute).toBe(120);
    });

    it('With right configuration, should generate right BPM for 3 steps, pair BPM difference', () => {
      // Arrange
      component.fullDuration = 150;
      component.stepDuration = 55;
      component.beginBpm = 100;
      component.finalBpm = 120;

      // Act
      component.generateSteps();

      // Assert
      expect(component.steps.length).toBe(3);
      expect(component.steps[0].beatsPerMinute).toBe(100);
      expect(component.steps[1].beatsPerMinute).toBe(110);
      expect(component.steps[2].beatsPerMinute).toBe(120);
    });

    it('With right configuration, should generate right BPM for 3 steps, odd BPM difference', () => {
      // Arrange
      component.fullDuration = 150;
      component.stepDuration = 55;
      component.beginBpm = 100;
      component.finalBpm = 119;

      // Act
      component.generateSteps();

      // Assert
      expect(component.steps.length).toBe(3);
      expect(component.steps[0].beatsPerMinute).toBe(100);
      expect(component.steps[1].beatsPerMinute).toBe(110);
      expect(component.steps[2].beatsPerMinute).toBe(119);
    });

    it('With right configuration, should generate right BPM for 4 steps, odd BPM difference', () => {
      // Arrange
      component.fullDuration = 200;
      component.stepDuration = 55;
      component.beginBpm = 100;
      component.finalBpm = 119;

      // Act
      component.generateSteps();

      // Assert
      expect(component.steps.length).toBe(4);
      expect(component.steps[0].beatsPerMinute).toBe(100);
      expect(component.steps[1].beatsPerMinute).toBe(107);
      expect(component.steps[2].beatsPerMinute).toBe(113);
      expect(component.steps[3].beatsPerMinute).toBe(119);
    });
  });
});
