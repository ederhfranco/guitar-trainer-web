import { Component, OnInit, ViewChild } from '@angular/core';
import { interval, Subscription } from 'rxjs';

interface Step {
  index: number;
  beatsPerMinute: number;
  duration: number;
} 

@Component({
  selector: 'app-automator',
  templateUrl: './automator.component.html',
  styleUrls: ['./automator.component.scss']
})
export class AutomatorComponent implements OnInit {

  beginBpm: number;
  finalBpm: number;
  fullDuration: number;
  stepDuration: number;

  decreaseTimer: number;
  stepRemainingTime: number;

  steps: Step[] = [];

  @ViewChild('metronome')
  metronome: any;

  constructor() { }

  ngOnInit(): void {
  }

  generateSteps() {
    if (this.fullDuration < this.stepDuration) {
      return;
    }

    this.steps = [];

    const amountOfSteps = Math.ceil(this.fullDuration / this.stepDuration);
    const lastStepDuration = this.fullDuration % this.stepDuration;
    let stepDurationToSet = this.stepDuration

    const bpmToIncrease = Math.floor((this.finalBpm - this.beginBpm) / (amountOfSteps - 1));
    const addToFirstBpmIncrease = ((this.finalBpm - this.beginBpm) % (amountOfSteps - 1));
    let bpmToSet = this.beginBpm;

    for (let index = 0; index < amountOfSteps; index++) {

      if (index + 1 === amountOfSteps && lastStepDuration > 0) {
        stepDurationToSet = lastStepDuration;
      }

      if (index + 1 === amountOfSteps) {
        bpmToSet = this.finalBpm;
      }

      const step: Step = {
        index: index + 1,
        beatsPerMinute: bpmToSet,
        duration: stepDurationToSet,
      }

      this.steps.push(step);

      if (index === 0) {
        bpmToSet += bpmToIncrease + addToFirstBpmIncrease;
      } else {
        bpmToSet += bpmToIncrease;
      }
    }

    this.metronome.setBpm(this.steps[0].beatsPerMinute);
    this.decreaseTimer = this.fullDuration;
    this.stepRemainingTime = this.steps[0].duration;
  }

  playSteps() {
    const source: Subscription = interval(1000)
      .subscribe(() => this.decreaseTimer -= 1);
    setTimeout(() => source.unsubscribe(), this.fullDuration * 1000);

    this.recursivePlay();
  }

  recursivePlay() {
    if (this.steps.length === 0) {
      this.metronome.stop();
      return;
    }

    this.metronome.setBpm(this.steps[0].beatsPerMinute);
    this.metronome.start();

    this.stepRemainingTime = this.steps[0].duration;
    const source: Subscription = interval(1000)
      .subscribe(() => this.stepRemainingTime -= 1);
    setTimeout(() => source.unsubscribe(), this.steps[0].duration * 1000);

    setTimeout(() => {
      this.steps.shift();
      this.recursivePlay();
    }, this.steps[0].duration * 1000);
  }

}
