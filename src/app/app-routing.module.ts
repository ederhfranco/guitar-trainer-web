import { MetronomeComponent } from './metronome/metronome.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AutomatorComponent } from './automator/automator.component';


const routes: Routes = [
  { path: 'metronome', component: MetronomeComponent },
  { path: 'automator', component: AutomatorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
